package com.myfirstspring.core;

public class HelloWorld {
	private String classicMessage;
	private String briefMessage;
	private String refMessage;
	private String refOutMessage;

	/** Getters & Setters **/
	public void setClassicMessage(String classicMessage){
		this.classicMessage  = classicMessage;
	}
	
	public void setBriefMessage(String briefMessage){
		this.briefMessage  = briefMessage;
	}
	
	public void setRefMessage(String refMessage){
		this.refMessage  = refMessage;
	}
	
	public void setRefOutMessage(String refOutMessage){
		this.refOutMessage  = refOutMessage;
	}

	public void getClassicMessage(){
		System.out.println("Your Message : " + classicMessage);
	}
	
	public void getBriefMessage(){
		System.out.println("Your Message : " + briefMessage);
	}
	
	public void getRefMessage(){
		System.out.println("Your Message : " + refMessage);
	}
	
	public void getRefOutMessage(){
		System.out.println("Your Message : " + refOutMessage);
	}
	
	public void getMessages(){
		System.out.println("Your Messages are: \n" + classicMessage +" \n" + briefMessage + "\n" + refMessage + "\n" + refOutMessage );
	}
}
