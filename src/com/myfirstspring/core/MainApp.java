package com.myfirstspring.core;
import org.joda.time.LocalTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {

	public static void main(String[] args) {
		
		/** spring config **/
		ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		
		LocalTime currentTime = new LocalTime();
		
		/** old style 
		HelloWorld hw=new HelloWorld();
		hw.setMessage("Hola Pepe! s�n les " + currentTime );
		hw.getMessage();
		**/
		
		/** spring style **/
		HelloWorld obj = (HelloWorld)context.getBean("helloWorld");
		obj.getMessages();
	}

}
